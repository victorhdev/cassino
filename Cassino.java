import java.util.Random;

import javax.swing.JOptionPane;

public class Cassino {
    public static void main(String[] args) {
        Random rand = new Random(); //Declare the object of random number
        String age = JOptionPane.showInputDialog("What's your age?"); //Verify the players age
        int age1 = Integer.parseInt(age); //Transform the string age in a integer
        if (age1 < 18 ) {
            age1 = 18 - age1;
            //Here, you do not need to create another variable,  just use the same variable, after the comparation
            System.out.println("You must be over 16 to play here, try again in " + age1 + " year(s).");
            System.exit(0);
        }
        String aposta = JOptionPane.showInputDialog("Bet in a number between 1 and 10"); //Open dialog with the number input
        int num = Integer.parseInt(aposta); //Parse the string APOSTA to the number
        if(num < 1 || num > 10) {
            System.out.println("Bet only in a number between 1 and 10");
            System.exit(0);
        }
        int n = rand.nextInt(10)+1;
        if (num == n) {
            System.out.println("You won the bet!");
        }else {
            System.out.println("Sorry, wrong bet!!");
        }
        
    }
}